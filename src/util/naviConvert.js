//百度坐标转高德（传入经度、纬度）
export function bd2gd(bd_lng, bd_lat) {
    var X_PI = Math.PI * 3000.0 / 180.0;
    var x = bd_lng - 0.0065;
    var y = bd_lat - 0.006;
    var z = Math.sqrt(x * x + y * y) - 0.00002 * Math.sin(y * X_PI);
    var theta = Math.atan2(y, x) - 0.000003 * Math.cos(x * X_PI);
    var gd_lng = z * Math.cos(theta);
    var gd_lat = z * Math.sin(theta);
    return {lng: gd_lng.toFixed(6), lat: gd_lat.toFixed(6)}
}
//高德坐标转百度（传入经度、纬度）
export function gd2bd(gd_lng, gd_lat) {
    var X_PI = Math.PI * 3000.0 / 180.0;
    var x = gd_lng, y = gd_lat;
    var z = Math.sqrt(x * x + y * y) + 0.00002 * Math.sin(y * X_PI);
    var theta = Math.atan2(y, x) + 0.000003 * Math.cos(x * X_PI);
    var bd_lng = z * Math.cos(theta) + 0.0065;
    var bd_lat = z * Math.sin(theta) + 0.006;
    return {
        lng: bd_lng.toFixed(6),
        lat: bd_lat.toFixed(6)
       
    };
}
