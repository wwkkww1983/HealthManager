/**
 * NetUitl 网络请求的实现
 * @author LH
 * @date 2018-09-19
 */
'use strict';
import React, {Component} from 'react';
import Toast from 'react-native-root-toast';
import {connect} from 'react-redux';


class NetUitl extends React.Component {

  

  //post请求
  /**
  *url :请求地址
  *data:参数
  *callback:回调函数
  */
  static  postFrom(url, data, callback) {
      var opt = {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        body:data
      };


      try {
       
              fetch(url, opt)
            .then((response) => response.text())
            .then((responseText) => {
              callback(JSON.parse(responseText));
            }).catch(error=>{
              Toast.show("服务器错误"+error);
            }).done();

        
      } catch (error) {

        Toast.show("服务器错误"+error);
        
      }
     
    }
  /**
  *url :请求地址
  *data:参数(Json对象)
  *callback:回调函数
  */
static post(url, data, callback) {
    var opt = {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    };
    try {
      
              fetch(url, opt)
              .then((response) => response.json())
              .then((responseJson) => {
              callback((responseJson));
              }).catch(error=>{
                Toast.show("服务器错误"+error);
              }).done();
          

    } catch (error) {
      Toast.show("服务器错误"+error);

      
    }
   
  }
  //get请求
  /**
  *url :请求地址
  *callback:回调函数
  */
  static  get(url, callback) {
    try {
         
              fetch(url)
              .then((response) => response.text())
              .then((responseText) => {
                callback(JSON.parse(responseText));
              }).catch(error=>{
                Toast.show("服务器错误"+error);
              }).done();

    

    } catch (error) {
      Toast.show("服务器错误"+error);



      
    }
      
    }

}





export default (NetUitl);

