import * as types from '../Actions/actionTypes';

const initState={

NetWorkOK:false

};
 

export default function checkNetwork(state=initState,action){

    switch (action.type) {
        case types.NETWORK_STATUS_OK:
             return {
                 ...state,
                 NetWorkOK:true

             };
            
            break;

         case types.NETWORK_STATUS_FALSE:
              return {
                  ...state,
                  NetWorkOK:false
              };
            break;
    
        default:
             return state;
            break;
    }


}