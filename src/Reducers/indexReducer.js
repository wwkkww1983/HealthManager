import {combineReducers} from "redux";
import netWorkReducer from './netWorkReducer';
import naviReducer from './naviReducer'

const rootReducer=combineReducers({
    Net:netWorkReducer,
    Nav:naviReducer
});


export default rootReducer;
