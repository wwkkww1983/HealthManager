import ROUTE from '../Router/pageRouter'
import {
    createNavigationReducer,
  } from 'react-navigation-redux-helpers';
 
// const navReducer = (state,action) => {
//     const newState = ROUTE.router.getStateForAction(action, state);
//     return newState || state;
// }


const navReducer=createNavigationReducer(ROUTE)

export default navReducer;