import React, {Component} from 'react';
import {Platform, StyleSheet,Alert, TouchableOpacity,TextInput,Text,Image,View} from 'react-native';

class checkPeelResult extends Component{
    constructor(props)
    {
        super(props);
        this.state={
            checkInfo:{
                machineid:124390097,
                locate:"肥东生活家广场",
                time:"2018-5-7 13:31",
                warnning:4,
                data:[{PH:4.5,isOK:false},
                      {SG:1.014,isOK:false},
                      {URO:15,isOK:true},
                      {BLO:'阴性',isOK:true},
                      {WBC:'阳性',isOK:false},
                      {PRO:'阴性',isOK:true},
                      {GLU:'阴性',isOK:true},
                      {BIL:'阴性',isOK:true},
                      {KET:'阳性',isOK:false},
                      {RBC:'阴性',isOK:true},
                      {GOL:'浅黄',isOK:true}
                     ]
            },
        }
    }

     static navigationOptions = {

        /**
         *  title：标题，如果设置了这个导航栏和标签栏的title就会变成一样的，不推荐使用
            header：可以设置一些导航的属性，如果隐藏顶部导航栏只要将这个属性设置为null
            headerTitle：设置导航栏标题，推荐
            headerBackTitle：设置跳转页面左侧返回箭头后面的文字，默认是上一个页面的标题。可以自定义，也可以设置为null
            headerTruncatedBackTitle：设置当上个页面标题不符合返回箭头后的文字时，默认改成"返回"
            headerRight：设置导航条右侧。可以是按钮或者其他视图控件
            headerLeft：设置导航条左侧。可以是按钮或者其他视图控件
            headerStyle：设置导航条的样式。背景色，宽高等
            headerTitleStyle：设置导航栏文字样式
            headerBackTitleStyle：设置导航栏‘返回’文字样式
            headerTintColor：设置导航栏颜色
            headerPressColorAndroid：安卓独有的设置颜色纹理，需要安卓版本大于5.0
            gesturesEnabled：是否支持滑动返回手势，iOS默认支持，安卓默认关闭
         */
       // title:'14项尿常规检测',
        headerTitle:'尿检报告',
        headerTruncatedBackTitle:"返回",
        //headerBackTitle:null,
        headerBackTitleStyle:{color:'white',fontSize:10},
        gesturesEnabled:true  // 是否可以右滑返回
     
       };




    render() {

        return <View style={styles.container}>
                     <View style={styles.first}>
                       <Image style={{height:20,width:20,left:10,resizeMode:"contain"}} source={require("../../res/images/locte.png")} ></Image>
                       <View style={{flexDirection:"column",flex:1,left:15}}>
                             <Text>设备编号：{this.state.checkInfo.machineid}</Text>
                             <Text>检测地点：{this.state.checkInfo.locate}</Text>
                             <Text>检测时间：{this.state.checkInfo.time}</Text>
                       </View>
                     
                       <TouchableOpacity style={{backgroundColor:'rgb(119,159,207)',borderRadius:10,right:10}} onPress={()=>{Alert.alert("duibibiao")}}>
                             <Text style={{color:'white',margin:5}}>医学参数说明</Text>
                       </TouchableOpacity>

                     </View>
                     <View style={styles.second}>
                       <View style={{flex:2,flexDirection:"column",justifyContent:"center",borderRightWidth:1,alignItems:'center'}}>
                            <View style={{flex:1,alignSelf:'center',justifyContent:'center'}}>
                                <Text style={{color:'black',fontSize:16,fontWeight:'bold',}}>检测结果</Text>
                            </View>
                            <View style={{flex:11,flexDirection:'row',justifyContent:"center",alignItems:'center'}}>
                                  <View style={[styles.cell,{borderRightWidth:1,borderTopWidth:1,}]}>
                                       <Text style={styles.cell}>酸碱度(PH)</Text>
                                       <Text style={styles.cell}>尿比重(SG)</Text>
                                       <Text style={styles.cell}>尿胆原(URO)</Text>
                                       <Text style={styles.cell}>隐血(BLO)</Text>
                                       <Text style={styles.cell}>白细胞(WBC)</Text>
                                       <Text style={styles.cell}>尿蛋白(PRO)</Text>
                                       <Text style={styles.cell}>尿糖(GLU)</Text>
                                       <Text style={styles.cell}>胆红素(BIL)</Text>
                                       <Text style={styles.cell}>酮体(KET)</Text>
                                       <Text style={styles.cell}>尿红细胞(RBC)</Text>
                                       <Text style={styles.cell}>尿液颜色(GOL)</Text>
                                  </View>
                                  <View style={{flex:1,borderTopWidth:1,backgroundColor:'rgb(228,231,232)'}}>
                                       <Text style={[styles.cell,{color:`${this.state.checkInfo.data[0].isOK?"green":"red"}`}]}>{this.state.checkInfo.data[0].PH}</Text>
                                       <Text style={[styles.cell,{color:`${this.state.checkInfo.data[1].isOK?"green":"red"}`}]}>{this.state.checkInfo.data[1].SG}</Text>
                                       <Text style={[styles.cell,{color:`${this.state.checkInfo.data[2].isOK?"green":"red"}`}]}>{this.state.checkInfo.data[2].URO}</Text>
                                       <Text style={[styles.cell,{color:`${this.state.checkInfo.data[3].isOK?"green":"red"}`}]}>{this.state.checkInfo.data[3].BLO}</Text>
                                       <Text style={[styles.cell,{color:`${this.state.checkInfo.data[4].isOK?"green":"red"}`}]}>{this.state.checkInfo.data[4].WBC}</Text>
                                       <Text style={[styles.cell,{color:`${this.state.checkInfo.data[5].isOK?"green":"red"}`}]}>{this.state.checkInfo.data[5].PRO}</Text>
                                       <Text style={[styles.cell,{color:`${this.state.checkInfo.data[6].isOK?"green":"red"}`}]}>{this.state.checkInfo.data[6].GLU}</Text>
                                       <Text style={[styles.cell,{color:`${this.state.checkInfo.data[7].isOK?"green":"red"}`}]}>{this.state.checkInfo.data[7].BIL}</Text>
                                       <Text style={[styles.cell,{color:`${this.state.checkInfo.data[8].isOK?"green":"red"}`}]}>{this.state.checkInfo.data[8].KET}</Text>
                                       <Text style={[styles.cell,{color:`${this.state.checkInfo.data[9].isOK?"green":"red"}`}]}>{this.state.checkInfo.data[9].RBC}</Text>
                                       <Text style={[styles.cell,{color:`${this.state.checkInfo.data[10].isOK?"green":"red"}`}]}>{this.state.checkInfo.data[10].GOL}</Text>                                 
                                  </View>

                            </View>

                       </View>
                       <View style={{flex:1,flexDirection:"column",justifyContent:"center",alignItems:'center'}}>
                                    <View style={{flex:1,alignSelf:'center',justifyContent:'center'}}>
                                        <Text style={{color:'black',fontSize:16,fontWeight:'bold'}}>常规值</Text>
                                    </View> 
                                    <View style={{flex:11,width:'100%',borderTopWidth:1,backgroundColor:'rgb(228,231,232)'}}>
                                       <Text style={styles.cell}>4.6～8.0(平均6.0)</Text>
                                       <Text style={styles.cell}>1.015～1.025</Text>
                                       <Text style={styles.cell}>{"<16"}</Text>
                                       <Text style={styles.cell}>阴性-</Text>
                                       <Text style={styles.cell}>阴性-</Text>
                                       <Text style={styles.cell}>阴性-或微量</Text>
                                       <Text style={styles.cell}>阴性-</Text>
                                       <Text style={styles.cell}>阴性-</Text>
                                       <Text style={styles.cell}>阴性-</Text>
                                       <Text style={styles.cell}>阴性-</Text>
                                       <Text style={styles.cell}>浅黄至深黄</Text>
                                   </View>                           
                       </View>
                     </View>

                     <View style={styles.third}>
                      <View style={{flexDirection:'row',marginLeft:10}}>
                        <Text>温馨提示：</Text>
                        <Text>异常项</Text>
                        <Text style={{color:'red',fontWeight:'bold',fontSize:16}}>{this.state.checkInfo.warnning}</Text>
                        <Text>项。</Text>
                      </View>
                      <Text style={{backgroundColor:'red',marginTop:5,color:'yellow',fontWeight:'bold',alignSelf:'center'}}>以上检测结果仅供参考，异常项建议就医复查!</Text>
                      
                    </View>

   

               </View>
    }



}

const styles=StyleSheet.create({
    container: {
        flex:1,
         backgroundColor:'#f3f3f4',

       
    },
    first:{
       flex:1,
      // margin: 10,
       flexDirection: 'row',
       alignItems: 'center',
       backgroundColor:"white",
       borderRadius: 2,
      // justifyContent: 'space-between',
      

    },
    cell:{
        flex:1,
        //backgroundColor:'white',
        justifyContent:"center",
        alignSelf: 'center',
        // borderLeftWidth: 1,
        // borderBottomWidth: 1,
    },
    second:{
        flex:6,
        flexDirection: 'row',
        margin: 10,
      borderRadius: 2,
       // borderBottomColor: 'gray',
        //borderBottomWidth: 1,
        backgroundColor:'white',
        borderWidth: 1

        
 
     },
     third:{
        flex:1,
        marginTop:-5,
       // marginLeft:10,
      //  justifyContent:"flex-start",
       // alignItems: 'center',
       
     },
    
});





export default checkPeelResult;
 