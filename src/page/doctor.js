import React from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    View,
    Text,
    FlatList,
    Platform,
    ScrollView,
    Alert,
    Image
} from 'react-native';
const HealthStatus={
    Good:0,
    Carefully:1,
    Badly:2

}
var Dimensions = require('Dimensions');
var {width, height} = Dimensions.get('window');
 class doctor extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            lastResult:[{name:'peel',
                         status:0,//健康状态：0-健康，1-有异常项
                         time:'2018/7/23'},
                         {name:'blood',
                         status:1,//健康状态：0-健康，1-有异常项
                         time:'2018/7/25'},
                         {name:'Ovu',
                         status:0,//健康状态：0-健康，1-有异常项
                         time:'2018/7/22'},
                         {name:'Preg',
                         status:1,//健康状态：0-健康，1-有异常项
                         time:'2018/6/23'} ],
             isRefresh:false
           
        };
  
        this.onPeelCheck= this.onPeelCheck.bind(this);
        this.onBloodCheck= this.onBloodCheck.bind(this);
        this.onOvulationCheck= this.onOvulationCheck.bind(this);
        this.onPregnantCheck= this.onPregnantCheck.bind(this);
    }
   
    componentDidMount(){
       
    }
   
    componentWillUnmount(){
    }
   
    onPeelCheck(){
        this.props.navigation.navigate('peel');
        }


    onBloodCheck(){
        this.props.navigation.navigate('blood');

    }

    onOvulationCheck(){
        this.props.navigation.navigate('ovu');


    }

    onPregnantCheck(){
        this.props.navigation.navigate('preg');
    }

    _renderIteam({item}){


        let title,image;
        if(item.name=="peel")
        {
            title="尿常规检测";
            image=<Image style={styles.imageSmall}                    
            source={require('../../res/images/peel.png')}>
            </Image>;
        }
        if(item.name=="blood")
        {
            title="血液检测";
            image=<Image style={styles.imageSmall}                   
            source={require('../../res/images/blood.png')}>
         </Image>;
        }

        if(item.name=="Ovu")
        {
            title="排卵检测";
            image=<Image style={styles.imageSmall}                    
            source={require('../../res/images/pailuan.png')}>
         </Image>;
        }
        if(item.name=="Preg")
        {
            title="早孕检测";
            image=<Image style={styles.imageSmall}                   
            source={require('../../res/images/huaiyun.png')}>
         </Image>;
        }
       
    return   <TouchableOpacity style={styles.item}>

                {image}

            <Text style={[styles.text,{flex:2,marginLeft:5,fontSize:14}]}>{title}</Text>
            <Text style={[styles.text,{flex:2,marginLeft:2,fontSize:12}]}>最近一次检测:</Text>
            <Text style={[styles.text,{flex:1,fontSize:16,color:`${item.status==0 ? "green" : "red"}`}]}>{`${item.status==0 ? "正常" : "异常"}`}</Text>
            <Text style={[styles.text,{flex:1.5,fontSize:12}]}>{item.time}</Text>
            {/* <TouchableOpacity style={{flex:1,marginRight:-5}} onPress={this.onLookDetail} >        
                <Text style={{fontSize:16}}>
                查看
                </Text>                               
            </TouchableOpacity>            */}
        </TouchableOpacity>

    }

    _refresh(){
       // Alert.alert("刷新中");
    }

    onLookDetail(){

    }
 
    render() {

     return <View style={styles.container}>
             {this.renderNavBar()}
            <Text>暂未开放</Text>     
            </View>
        
    }

    renderNavBar(){
        return(
            <View style={styles.navBarStyle}>
                <Text style={[styles.navTitleStyle]}>
                    健康咨询
                </Text>
            </View>
        )
    }


 }


const styles = StyleSheet.create({
   
    container: {
        flex:1,
      //  top:Platform.OS==='ios'?20:0,
       // bottom:100
    },
    funcarea:{
        flex:1,
    },
    resultarea:{
        flex:1,
    },
    func1:{
        flex:1,
        flexDirection: 'row',
        borderBottomColor: 'gray',
        borderBottomWidth: 1,
        borderTopColor: 'gray',
        borderTopWidth: 1,
    },
    func2:{
        flex:1,
        flexDirection: 'row',
        borderBottomColor: 'gray',
        borderBottomWidth: 1,
    },
     text:{
        fontSize:10,
        color:'gray'
     },
     imageBig:{
        width:60,
        height:60,
        resizeMode:'contain',
        alignSelf: 'center',  
     },
     imageSmall:{         
         flex:1,
         resizeMode:'contain',
         width:40,
         height:40,
         left:5
     },
     navBarStyle:{ // 导航条样式
        height:50,
        backgroundColor:'rgb(30,70,125)',
        // 设置主轴的方向
        flexDirection:'row',
        // 垂直居中 ---> 设置侧轴的对齐方式
        alignItems:'center',
        justifyContent:'center'
    },
    navTitleStyle: {
        color:'white',
        fontSize:16,
        top:10
    },
    item:{
        //flex:1,
        height:60,
        flexDirection:'row',
        backgroundColor:"white",
        borderBottomColor: 'gray',
        borderBottomWidth: 1,
        alignItems: 'center',
    }  
});
    

export default doctor;