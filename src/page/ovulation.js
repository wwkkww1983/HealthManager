import React, {Component} from 'react';
import {Platform, StyleSheet, TouchableOpacity,TextInput,Text,Image,View} from 'react-native';
import qrScanner from '../component/qrScanner'

class ovulation extends Component{
    constructor(props)
    {
        super(props);
        this.state={
            total:5,
        }
    }

     static navigationOptions = {

        /**
         *  title：标题，如果设置了这个导航栏和标签栏的title就会变成一样的，不推荐使用
            header：可以设置一些导航的属性，如果隐藏顶部导航栏只要将这个属性设置为null
            headerTitle：设置导航栏标题，推荐
            headerBackTitle：设置跳转页面左侧返回箭头后面的文字，默认是上一个页面的标题。可以自定义，也可以设置为null
            headerTruncatedBackTitle：设置当上个页面标题不符合返回箭头后的文字时，默认改成"返回"
            headerRight：设置导航条右侧。可以是按钮或者其他视图控件
            headerLeft：设置导航条左侧。可以是按钮或者其他视图控件
            headerStyle：设置导航条的样式。背景色，宽高等
            headerTitleStyle：设置导航栏文字样式
            headerBackTitleStyle：设置导航栏‘返回’文字样式
            headerTintColor：设置导航栏颜色
            headerPressColorAndroid：安卓独有的设置颜色纹理，需要安卓版本大于5.0
            gesturesEnabled：是否支持滑动返回手势，iOS默认支持，安卓默认关闭
         */
          // title:'排卵检测',
           headerTitle:'排卵检测',
        //    headerTitleStyle:{color:'white',fontSize:16},
        //    headerBackTitleStyle:{color:'white',fontSize:10},
        //    headerStyle:{backgroundColor:'rgb(30,70,125)',color:'white',height:40,fontSize:16},
        //    //headerBackTitle:null,
        //    headerBackTitleStyle:{color:'white',fontSize:10},
           gesturesEnabled:true  // 是否可以右滑返回
     
       };




    render() {

        return <View style={styles.container}>
                     <View style={[styles.first,{flex:0.3}]}>
                        <Image style={[styles.imageSmall,{flex:1,alignSelf:'center'}]}                    
                            source={require('../../res/images/pailuan.png')}>
                        </Image>
                        <View style={{flex:1,flexDirection:'row',alignItems:'center',justifyContent:'center'}}>
                            <Text style={{color:'gray',fontWeight:'bold',fontSize:12}}>历史记录</Text>
                            <Text style={{color:'green',fontSize:20}}>{this.state.total}</Text>
                            <Text style={{color:'gray',fontWeight:'bold',fontSize:12}}>次</Text>
                        </View>
                     </View>
                     <View style={styles.first}>
                        <View style={{flex:1,alignItems:'center',justifyContent:'center',borderRightColor:'gray',borderRightWidth:1}}>
                            <TouchableOpacity style={{alignItems:'center',justifyContent:'center'}} onPress={()=>{ this.props.navigation.navigate('qr',{ title: '尿常规检测' });}}>
                                <Image style={[styles.imageSmall,{height:80,width:80,flex:3}]}                   
                                    source={require('../../res/images/swipe.png')}>
                                </Image>
                                { <Text style={{flex:0.5,fontWeight:'bold',color:'gray',fontSize:12}}>扫码</Text> }
                            </TouchableOpacity >
                         </View>  

                         {/* <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
                         <TouchableOpacity style={{alignItems:'center',justifyContent:'center'}} onPress={null}>
                            <Image style={[styles.imageSmall,{height:50,width:50,flex:3}]}                    
                                source={require('../../res/images/swipe.png')}>
                            </Image>
                            <Text style={{flex:1,fontWeight:'bold',color:'gray',fontSize:12}}>检测</Text>
                         </TouchableOpacity>
                         </View> */}
                     </View>
                     <View style={styles.second}>
                     <Text style={{left:15,color:'rgb(81,151,244)',fontWeight:'bold',fontSize:16}}>检测教程</Text>
                    </View>
                     <View style={styles.third}>
                          <Text style={styles.text}>第一步：手机扫出纸码获取化学反应试纸</Text>
                          <Text style={styles.text}>第二步：撕开包装袋，获取试纸</Text>
                          <Text style={styles.text}>第三步：用试纸蘸取尿液样本</Text>
                          <Text style={styles.text}>第四步：将蘸有尿液样本的试纸放入检测托盘中，扫检测码</Text>
                          <Text style={styles.text}>第五步：扫完离开即可，结果自动发送到App中</Text>
                          <Text style={styles.text}>第六步：查看检查结果</Text>
                     </View>



               </View>;
    }



}

const styles=StyleSheet.create({
    container: {
        flex:1,
        backgroundColor:'white',

       
    },
    first:{
       flex:0.6,
       flexDirection: 'row',
       justifyContent: 'center',
       alignItems: 'center',
       borderBottomColor: 'gray',
       borderBottomWidth: 1,

    },
    second:{
        flex:0.3,
        justifyContent:"center",
        alignItems: 'flex-start',
        borderBottomColor: 'gray',
        borderBottomWidth: 1,
 
     },
     third:{
        flex:2,
        justifyContent: 'space-between',
        left:15,
        top:10

     },
     imageSmall:{
         height:60,
         width:60,
         resizeMode:'contain',
         alignSelf: 'center',
        
     },
     text:{
        flex:1,fontWeight:'bold',color:'gray',fontSize:12
       
    }


});





export default ovulation;
 