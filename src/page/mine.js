import React, {Component} from 'react';
import {Platform,ScrollView,Dimensions,StatusBar,Alert,StyleSheet, TouchableOpacity,TextInput,Text,Image,Button,View} from 'react-native';
import Toast from 'react-native-root-toast'; 

import ViewUtil from '../util/ViewUtils'
import ParallaxScrollView from 'react-native-parallax-scroll-view'

const window = Dimensions.get('window');

const AVATAR_SIZE = 120;
const ROW_HEIGHT = 60;
const PARALLAX_HEADER_HEIGHT = 500;
const STICKY_HEADER_HEIGHT = 50;

class mine extends Component{
    static navigationOptions = {
     header:null
    };

    onClick(tap){
        Toast.show("内测版功能暂未开放,请等待正式版");

    }

    createSettingItem(tag,icon,text){
         return ViewUtil.createSettingItem(()=>this.onClick(tag),icon,text,null,null);
    }

    render(){
      
       return <View style={styles.main}>
                 {/* <StatusBar  
                    animated={true} //指定状态栏的变化是否应以动画形式呈现。目前支持这几种样式：backgroundColor, barStyle和hidden  
                    hidden={false}  //是否隐藏状态栏。  
                    // backgroundColor={'green'} //状态栏的背景色  
                     translucent={true}//指定状态栏是否透明。设置为true时，应用会在状态栏之下绘制（即所谓“沉浸式”——被状态栏遮住一部分）。常和带有半透明背景色的状态栏搭配使用。  
                    barStyle={'light-content'} // enum('default', 'light-content', 'dark-content')   
                                >  
                </StatusBar>   */}

                <ParallaxScrollView
                    backgroundColor="rgb(30,70,125)"
                    renderStickyHeader={()=> (
                        <View key="sticky-header" style={styles.stickySection}>
                            <Text style={styles.stickySectionText}>用户中心</Text>
                        </View>
                    )}
                    stickyHeaderHeight={ STICKY_HEADER_HEIGHT }
                    renderBackground={()=>(// key="background"
                        <View>
                            <Image style={styles.imageback} source={require("../../res/images/timg.jpg")}/>
                        </View>
                    )
                }
                    contentBackgroundColor="#f3f3f4"
                    parallaxHeaderHeight={300}
                    
                    renderForeground={() => (
                    <View style={{ alignItems: 'center',
                    flex: 1,
                    flexDirection: 'column',
                    paddingTop: 100 }}>
                       <Image style={styles.image} source={require("../../res/images/timg2.jpeg")}></Image>
                       <View style={{flexDirection:'row'}}>
                            <TouchableOpacity>
                                 <Image style={{height:40,width:40,resizeMode:'contain'}} source={require("../../res/images/logo.png")}></Image>
                                 <Text></Text>   

                            </TouchableOpacity>
                       </View>
                    </View>

                    )}>
                    <View style={{flex:1}}>
                         {/*=============趋势管理Section=============*/}
                         <Text style={{marginTop:10}}></Text>
                        <View style={styles.cellBottomLineStyle}></View>
                        {/*自定义语言*/}
                        {this.createSettingItem('账户',require('../../res/images/save.png'),"安全保障")}
                        <View style={styles.cellBottomLineStyle}></View>


                        {/*语言排序*/}
                        {this.createSettingItem('MORE_MENU.Sort_Language',require('../../res/images/help.png'),'帮助中心')}
                        <View style={styles.cellBottomLineStyle}></View>

                        {/*=============标签管理Section=============*/}
                        <Text style={styles.groupTitleStyle}></Text>

                        <View style={styles.cellBottomLineStyle}></View>
                        {/*自定义标签*/}
                        {this.createSettingItem('MORE_MENU.Custom_Key',require('../../res/images/about.png'),'关于我们')}
                        <View style={styles.cellBottomLineStyle}></View>
                        {/*标签排序*/}
                        {this.createSettingItem('MORE_MENU.Sort_Key',require('../../res/images/update.png'),'检查更新')}
                        <View style={styles.cellBottomLineStyle}></View>

                        {/*标签移除*/}
                        {this.createSettingItem('MORE_MENU.Remove_Key',require('../../res/images/service.png'),'联系客服')}
                        <View style={styles.cellBottomLineStyle}></View>

                        {/*=============设置Section=============*/}
                        <Text style={styles.groupTitleStyle}></Text>
                        {/*自定义主题*/}
                        <View style={styles.cellBottomLineStyle}></View>
                        {this.createSettingItem('MORE_MENU.Custom_Theme',require('../../res/images/exit.png'),'退出登录')}
                        <View style={styles.cellBottomLineStyle}></View>

                        
                    </View>
                 </ParallaxScrollView>

             </View>

    }

}

const styles=StyleSheet.create({

    main:{
       flex:1,
       backgroundColor:'#f3f3f4',
       // bottom:60
    },
    image:{
        height:AVATAR_SIZE,
        width:AVATAR_SIZE,
        borderRadius:60
       // resizeMode:'contain',
        //alignSelf: 'center',
    }, 
    imageback:{
        width: '100%',
        height: 300,
       // resizeMode:'contain',
    }, 
     cell: {
        flex: 1,
        backgroundColor: 'white',
        padding: 10,
        marginLeft: 5,
        marginRight: 5,
        marginVertical: 3,
        borderColor: '#dddddd',
        borderStyle: null,
        borderWidth: 0.5,
        borderRadius: 2,
        shadowColor: 'gray',
        shadowOffset: {width:0.5, height: 0.5},
        shadowOpacity: 0.4,
        shadowRadius: 1,
        elevation:2
    },
     cellBottomLineStyle: {
        height: 0.4,
        opacity:0.5,
        backgroundColor: 'darkgray',
    },
    itemInfoItemStyle:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        padding:10,
        height:76,
        backgroundColor:'white'
    },

    groupTitleStyle:{
        marginLeft:10,
        marginTop:15,
        marginBottom:6,
        color:'gray'
    },
    stickySection: {
        height: STICKY_HEADER_HEIGHT,
        justifyContent: 'center',
        alignItems:'center',
       // paddingTop:(Platform.OS === 'ios')?20:0,
    },
    stickySectionText: {
        color: 'white',
        fontSize: 20,
        margin: 10
    }



});

export default mine;