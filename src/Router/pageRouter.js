
import React, {Component} from 'react';
import {Platform,View} from 'react-native';
import { createStackNavigator } from 'react-navigation';
import App from '../../App'
import mainPage from "../page/mainPage"
import registerPage from '../page/register'
import forgetPassPage from '../page/forgetPassWord'
import peelPage from '../page/peel'
import checkPage from '../page/check'
import qrPage from '../component/qrScanner'
import bloodPage from '../page/blood'
import ovuPage from '../page/ovulation'
import pregPage from '../page/pregnant'
import checkPeelResultPage from '../page/checkPeelResult'
import {connect} from 'react-redux';



import {
    reduxifyNavigator,
    createNavigationReducer,
    createReactNavigationReduxMiddleware,
} from 'react-navigation-redux-helpers';


const route =createStackNavigator({
    login:{screen:App},
    main:{screen:mainPage},
    register:{screen:registerPage},
    frogetPass:{screen:forgetPassPage},
    peel:{screen:peelPage},
    check:{screen:checkPage},
    qr:{screen:qrPage},
    blood:{screen:bloodPage},
    ovu:{screen:ovuPage},
    preg:{screen:pregPage},
    checkPeelResult:{screen:checkPeelResultPage}
    
  },
  {
    navigationOptions:{ 
       headerTitleStyle:{color:'white',fontSize:16, top:Platform.OS==='ios'?0:10,flex:1,textAlign: 'center'},
       headerBackTitleStyle:{color:'white'},
       headerStyle:{backgroundColor:'rgb(30,70,125)',color:'white',height:50,fontSize:16},
      //  headerBackTitle:'尿常规检测',
       headerTintColor:'#fff',
       headerRight:<View/>,
    }
  }
  );

export default route
 