import * as types from './actionTypes'

export function setNetStatus(status)
{

    return (dispatch)=>{

        console.log("setNetSatus:"+status);
        

        if(status)
        {
            dispatch({
                type:types.NETWORK_STATUS_OK
            });
    
        }else{
            dispatch({
                type:types.NETWORK_STATUS_FALSE
            });
        }


    }

}