import {createStore,applyMiddleware} from 'redux';
import thunkMiddleware  from 'redux-thunk';
import indexReducer from '../Reducers/indexReducer';
import route from '../Router/pageRouter'
import {
    reduxifyNavigator,
    createNavigationReducer,
    createReactNavigationReduxMiddleware,
} from 'react-navigation-redux-helpers';

const middleware = createReactNavigationReduxMiddleware(
    "root",
    state => state.Nav,
);
const middleWares = [middleware,thunkMiddleware];
export default applyMiddleware(...middleWares)(createStore)(indexReducer);


// export default function initStore(initState){

//     const store=createStoreWithMidd(indexReducer,initState);
//     return store;

// };