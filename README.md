<!--
 * @Description: file content
 * @Version: 1.0
 * @Author: LiangHui
 * @Date: 2020-08-17 16:24:27
 * @LastEditors: LiangHui
 * @LastEditTime: 2020-08-22 15:44:41
 * @FilePath: /HealthManager/README.md
-->
# HealthManager

#### 项目介绍
一款LBS应用，查找最近的体液检测设备，导航到该地点，在设备上扫码出试纸，进行体液检测，检测健康状态，设计功能支持尿常规检测，无创血液检测，排卵检测，早孕检测（检测设备已研发，项目计划投放到写字楼，车站、机场卫生间，小区等地点解决普通人的常规体检需求，如农民工、环卫工等等这些可能从来没有去医院体检过的人能有机会进行一些必要的常规体检，了解自身健康状态，将重大疾病扼杀在摇篮中。因缺少资金，未真实上线，系统显示的检测设备均为后台模拟数据）。
#### 软件架构
React-Native 开发，支持Android/IOS。
#### 安装教程

1. 根据你的系统自行安装React-Native开发环境 
2. Clone 本仓库代码
3. 进入根目录下
4. npm install
5. npm run android 或者 npm run ios

正常情况下会启动模拟器运行app，注意由于引用的第三库如百度地图插件版本较低，release无法正常运行，故以debug代替发行版，代码中已关闭显示warning和弹出debug菜单功能，你将无法使用摇一摇或者cmd+M（mac）弹出debug设置菜单，如需开启请更改以下代码
##
```
android/src/main/java/com/healthmanager/MainApplication.java中还原如下代码："return false;"改为"return BuildConfig.DEBUG;"
private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return false;//BuildConfig.DEBUG;//关闭真机debug包摇一摇出现debug菜单
    }
   
 ```

 如果出现打包后图片丢失的情况，请执行
 npm run bundle-android 或者在根目录下执行：
```
react-native bundle --entry-file index.js --platform android --dev false --bundle-output ./android/app/src/main/assets/index.android.bundle --assets-dest ./android/app/src/main/res/
```
 
 生成资源。

 #### 关于服务端
  由于服务端含有保密信息，尚未完成脱敏故暂时不开放服务端代码，
 App已连接后台服务，直接可以测试使用，测试账号：18888888888 密码：123456


#### App 使用截图

- 登录页
![输入图片说明](https://images.gitee.com/uploads/images/2020/0822/145310_6f1f3a1e_1444206.jpeg "Screenshot_20200822_125143_com.healthmanager.jpg")


- 检测页面，有四个健康检测项目，尿常规，无创血液检测，排卵检测，早孕检测，终端设备上已经支持检测
![输入图片说明](https://images.gitee.com/uploads/images/2020/0822/145325_fa319fe8_1444206.jpeg "Screenshot_20200822_125202_com.healthmanager.jpg")


- 地图页面-首次进入地图页面会请求位置权限
![输入图片说明](https://images.gitee.com/uploads/images/2020/0822/145344_53c174aa_1444206.jpeg "Screenshot_20200822_125215_com.healthmanager.jpg")

- 地图页面-默认显示当前城市设备热点
![输入图片说明](https://images.gitee.com/uploads/images/2020/0822/145405_a09bfaf5_1444206.jpeg "Screenshot_20200822_125239_com.healthmanager.jpg")

- 点击右上角“离我最近”，后台会计算离你最近的设备，点击设备热点后会有详情，底部会弹出“导航”按钮
![输入图片说明](https://images.gitee.com/uploads/images/2020/0822/145426_98d062fe_1444206.jpeg "Screenshot_20200822_125311_com.healthmanager.jpg")

- 点击导航按钮后会显示百度/高德地图的按钮，点击后启动对应的导航软件，导航到所点击的设备位置
![输入图片说明](https://images.gitee.com/uploads/images/2020/0822/145444_1910a342_1444206.jpeg "Screenshot_20200822_125329_com.healthmanager.jpg")

- 检测页面中“尿常规检测”页面，点击扫码按钮会启动摄像头扫码
![输入图片说明](https://images.gitee.com/uploads/images/2020/0822/145508_6d7476ac_1444206.jpeg "Screenshot_20200822_125415_com.healthmanager.jpg")

- 扫码状态
![输入图片说明](https://images.gitee.com/uploads/images/2020/0822/145528_dae6597c_1444206.jpeg "Screenshot_20200822_125625_com.healthmanager.jpg")

- 扫码成功后，终端设备会进行检测，检测结果会立即返回（图示为模拟数据）
![输入图片说明](https://images.gitee.com/uploads/images/2020/0822/145543_6a0cab68_1444206.jpeg "Screenshot_20200822_125639_com.healthmanager.jpg")

- 用户页
![输入图片说明](https://images.gitee.com/uploads/images/2020/0822/145600_be40ad90_1444206.jpeg "Screenshot_20200822_125726_com.healthmanager.jpg")












