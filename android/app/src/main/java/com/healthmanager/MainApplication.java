/*
 * @Description: file content
 * @Version: 1.0
 * @Author: LiangHui
 * @Date: 2020-08-17 16:24:27
 * @LastEditors: LiangHui
 * @LastEditTime: 2020-08-22 11:09:53
 * @FilePath: /HealthManager/android/app/src/main/java/com/healthmanager/MainApplication.java
 */
package com.healthmanager;
import android.app.Application;
import com.facebook.react.ReactApplication;
import com.github.wumke.RNExitApp.RNExitAppPackage;
import cn.qiuxiang.react.baidumap.BaiduMapPackage;
import org.reactnative.camera.RNCameraPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.showlocationservicesdialogbox.LocationServicesDialogBoxPackage;


import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return false;//BuildConfig.DEBUG;//关闭真机debug包摇一摇出现debug菜单
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
          new MainReactPackage(),
            new RNExitAppPackage(),
            new BaiduMapPackage(),
           // new BaiduMapPackage(getApplicationContext()),
            new RNCameraPackage(),
            new LocationServicesDialogBoxPackage() // <== this

      );
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
  }
}
