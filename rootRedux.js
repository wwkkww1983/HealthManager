import React, {Component} from 'react';
import{BackHandler} from 'react-native';
import {connect,Provider,applyMiddleware} from 'react-redux'
import initStore from './src/Store/store'
import Router from './src/Router/pageRouter'
import { NavigationActions } from 'react-navigation'
import {
	reduxifyNavigator,
	createReactNavigationReduxMiddleware,
	createNavigationReducer
  } from 'react-navigation-redux-helpers'

const addListener = reduxifyNavigator(Router,"root");
const mapStateToProps = state => {
	return {
	  state: state.Nav
	}
  }
  
  const HighOrderAppNavigation = connect(mapStateToProps)(addListener)


export default class RootApp extends Component {

	// componentDidMount () {
	// 	BackHandler.addEventListener('hardwareBackPress', this.onBackPress)
	//   }
	
	//   componentWillUnmount () {
	// 	BackHandler.removeEventListener('hardwareBackPress', this.onBackPress)
	//   }
	
	//   onBackPress = () => {

	// 		const { dispatch, nav } = this.props;
	// 		console.log(this.props)
	// 		// if (nav.index === 0) {
	// 		// return false;
	// 		// }

	// 	   initStore.dispatch(NavigationActions.back())
	// 		return true;
	// 	//initStore.dispatch(NavigationActions.back())
	// 	//return true
	//   };
 
	render() {
		return (
			<Provider store={initStore}>
				<HighOrderAppNavigation />
			</Provider >
		)
	}
}


