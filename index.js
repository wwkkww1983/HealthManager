/*
 * @Description: file content
 * @Version: 1.0
 * @Author: LiangHui
 * @Date: 2018-08-15 09:25:41
 * @LastEditors: LiangHui
 * @LastEditTime: 2020-08-22 00:10:29
 * @FilePath: /HealthManager/index.js
 */
/** @format */

import {AppRegistry} from 'react-native';
import {name as appName} from './App.json';
import RootApp from './rootRedux'
console.ignoredYellowBox = ['Warning: BackAndroid is deprecated. Please use BackHandler instead.','source.uri should not be an empty string','Invalid props.style key'];
console.disableYellowBox = true // 关闭全部黄色警告
AppRegistry.registerComponent(appName, () => RootApp);
